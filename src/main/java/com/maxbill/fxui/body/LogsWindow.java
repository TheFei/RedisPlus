package com.maxbill.fxui.body;

import com.maxbill.base.bean.Message;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;

import static com.maxbill.fxui.body.BodyWindow.getTabPane;
import static com.maxbill.fxui.util.CommonConstant.TEXT_TAB_LOGS;

/**
 * 日志窗口
 *
 * @author MaxBill
 * @date 2019/07/10
 */
public class LogsWindow {

    private static Tab logsTab;
    private static TextArea logArea;


    public static void initLogsTab() {
        if (null == logsTab) {
            logsTab = new Tab(TEXT_TAB_LOGS);
            getTabPane().getTabs().add(logsTab);
        }
        getTabPane().getSelectionModel().select(logsTab);
        if (null == logArea) {
            logArea = new TextArea();
            logArea.setWrapText(true);
            logArea.setEditable(false);
            logsTab.setContent(logArea);
            logsTab.setOnClosed(event -> {
                logArea = null;
                logsTab = null;
            });
        }
    }

    public static void showLog(Message message) {
        if (null != logArea) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("\r\n");
            stringBuilder.append(" ");
            stringBuilder.append(message.getTime());
            stringBuilder.append(" ");
            stringBuilder.append(message.getType());
            stringBuilder.append(" ");
            stringBuilder.append(message.getClassName());
            stringBuilder.append(" :");
            stringBuilder.append(message.getBody());
            stringBuilder.append("\r\n");
            logArea.appendText(stringBuilder.toString());
        }
    }
}
