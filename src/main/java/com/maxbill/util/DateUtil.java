package com.maxbill.util;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @功能 响应实体
 * @作者 MaxBill
 * @时间 2018年7月16日
 * @邮箱 maxbill1993@163.com
 */
public class DateUtil {

    private final static String DATE_STR = "yyyy-MM-dd";
    private static final String DATE_STR_FULL = "yyyy-MM-dd HH:mm:ss";


    /**
     * ----------------------日期格式化操作----------------------
     */

    public static String formatDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_STR);
        return format.format(date);
    }

    public static String formatDateTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_STR_FULL);
        return format.format(date);
    }

    public static String formatDate(Date date, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.format(date);
    }

    /**
     * ----------------------日期转换操作----------------------
     */

    public static String timeStampToDate(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat sd = new SimpleDateFormat(DATE_STR);
        return sd.format(date);
    }

    public static String timeStampToDateTime(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat sd = new SimpleDateFormat(DATE_STR_FULL);
        return sd.format(date);
    }

    public static String timeStampToDate(long timeStamp, String format) {
        Date date = new Date(timeStamp);
        SimpleDateFormat sd = new SimpleDateFormat(format);
        return sd.format(date);
    }


    public static String unixTimeStampToDate(long timeStamp) {
        Date date = new Date(timeStamp * 1000);
        SimpleDateFormat format = new SimpleDateFormat(DATE_STR);
        return format.format(date);
    }

    public static String unixTimeStampToDateTime(long timeStamp) {
        Date date = new Date(timeStamp * 1000);
        SimpleDateFormat format = new SimpleDateFormat(DATE_STR_FULL);
        return format.format(date);
    }

}
