package com.maxbill.base.bean;


import lombok.Data;

@Data
public class DataNode {

    private String name;

    private String type;

    private String size;

    private String time;

    private Object data;

    public DataNode(String name) {
        this.name = name;
    }

}
